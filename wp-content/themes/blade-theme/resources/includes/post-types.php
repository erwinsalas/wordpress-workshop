<?php

if (function_exists('acf_add_local_field_group')) {
    function create_product_post_type(){
        register_post_type(
          'product',
          array(
              'labels' => array(
                  'name' => __('Product'),
                  'singular_name' => __('Product'),
                  'plural_name' => __('Products')

              ),
              'menu_icon' => 'dashicons-admin-page',
              'public' => true,
              'has_archive' => false,
              'hierarchical' => true,
              'capability_type' => 'page',
              'show_in_rest' => true,
              'rewrite' => array('slug' => 'products'),
              'supports' => array(
                  'title',
                  'editor',
                  'page-attributes',
                  'thumbnail',
                  'custom-fields',
              )
          )
      );
  
        acf_add_local_field_group(array(
          'key' => 'product_field_group',
          'title' => 'Product Fields',
          'fields' => array(
              // Image
            array(
                'key' => 'product_image_field',
                'label' => 'Product Image',
                'type' => 'image',
                'name' => 'product_image',
                'placeholder' => 'Product Image',
            ),
            array(
                'key' => 'name_field',
                'label' => 'Product Name',
                'name' => 'product_name',
                'type' => 'text',
                'placeholder' => 'Product Name',
            ),
            array(
                'key' => 'description_field',
                'label' => 'Description',
                'name' => 'description',
                'type' => 'text',
                'placeholder' => 'Description',
            ),
            array(
                'key' => 'price_field',
                'label' => 'Price',
                'type' => 'number',
                'name' => 'price',
                'placeholder' => 'Price',
              ),
          ),
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => 'product'
                  ),
              ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'hide_on_screen' => '',
          'active' => true,
          'description' => '',
      ));
    }

    add_action('init', 'create_product_post_type');

    if (function_exists('acf_add_local_field_group')) {
        function create_places_post_type(){
            register_post_type(
              'place',
              array(
                  'labels' => array(
                      'name' => __('Place'),
                      'singular_name' => __('Place'),
                      'plural_name' => __('Places')
    
                  ),
                  'menu_icon' => 'dashicons-admin-page',
                  'public' => true,
                  'has_archive' => false,
                  'hierarchical' => true,
                  'capability_type' => 'page',
                  'show_in_rest' => true,
                  'rewrite' => array('slug' => 'places'),
                  'supports' => array(
                      'title',
                      'editor',
                      'page-attributes',
                      'thumbnail',
                      'custom-fields',
                  )
              )
          );
      
            acf_add_local_field_group(array(
              'key' => 'place_field_group',
              'title' => 'Place Fields',
              'fields' => array(
                  // Image
                array(
                    'key' => 'place_image_field',
                    'label' => 'Place Image',
                    'type' => 'url',
                    'name' => 'image',
                    'placeholder' => 'place Image',
                ),
                array(
                    'key' => 'place_vehicle_type_field',
                    'label' => 'Vehicle type',
                    'name' => 'vehicle_type',
                    'type' => 'text',
                    'placeholder' => 'Vehicle type',
                ),
                array(
                    'key' => 'place_accessibility_field',
                    'label' => 'Accessibility',
                    'name' => 'accessibility',
                    'type' => 'text',
                    'placeholder' => 'Accessibility',
                ),
                array(
                    'key' => 'place_distance_field',
                    'label' => 'Distance',
                    'name' => 'distance',
                    'type' => 'text',
                    'placeholder' => 'Distance',
                ),
                array(
                    'key' => 'place_cost_field',
                    'label' => 'Cost',
                    'name' => 'cost',
                    'type' => 'text',
                    'placeholder' => 'Cost',
                ),
                array(
                    'key' => 'place_waze_link_field',
                    'label' => 'Waze link',
                    'name' => 'waze_link',
                    'type' => 'url',
                    'placeholder' => 'Waze link',
                ),
              ),
              'location' => array(
                  array(
                      array(
                          'param' => 'post_type',
                          'operator' => '==',
                          'value' => 'place'
                      ),
                  ),
              ),
              'menu_order' => 0,
              'position' => 'normal',
              'style' => 'default',
              'label_placement' => 'top',
              'instruction_placement' => 'label',
              'hide_on_screen' => '',
              'active' => true,
              'description' => '',
          ));
        }
    
        add_action('init', 'create_places_post_type');
    }
}
