# Dockerized Wordpress
Setup a docker environment for Wordpress with PHP 7.2, and MySQL 5.7. This docker setup is to be used for local development and testing.

## Usage

Start initial container setup:
```bash
$ docker-compose up -d
```

## Configuration
| Database ||
|----------	| ----------------- |
| Host      | `wordpress_mysql` |
| Username  | `root`            |
| Password  | `abcd1234`        |
| Database  | `wordpress`       |

## License
This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)